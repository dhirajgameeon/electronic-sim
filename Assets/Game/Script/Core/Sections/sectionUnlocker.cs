using UnityEngine;

[System.Serializable]
public class sectionUnlocker
{
    public bool isLocked = false;
    public int UnlockingLevel = 4;
    public GameObject Locked;
    public GameObject Unlocked;
    public ParticleSystem unlockPartical;
    public Animator cinemachineCamera;
    public string stateName;
    public float unlockDelay = 1;

    public sectionUnlocker()
    {
        isLocked = false;
        UnlockingLevel = 4;
        Locked = null;
        Unlocked = null;
        unlockPartical = null;
        cinemachineCamera = null;
        stateName = "";
        unlockDelay = 1;
    }
}
