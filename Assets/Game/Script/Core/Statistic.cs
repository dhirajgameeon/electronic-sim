using UnityEngine;

[System.Serializable]
public class Statistic
{
    public int Level;
    public float HP;
    public float moveSpeed;
    public float attackMoveSpeed;
    public float AttackRange;
    public float AttackSpeed;
    public float AttackRate;

    public Statistic()
    {
        Level = 1;
        HP = 100;
        moveSpeed = 10;
        attackMoveSpeed = 1;
        AttackRange = 5;
        AttackSpeed = 1;
        AttackRate = 1;
    }

}
