using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Unlocker : MonoBehaviour
{
    [Header("Locked")]
    [Space(10)]
    public GameObject LockedObject;
    public ParticleSystem ParticalEffectForUnlocking;
    public TextMeshPro cashCounterText;
    public int ID = 8;
    public float unlockableAmount = 100;
    public int reduceAmountSpeed = 10;

    [Header("Unlocked")]
    [Space(10)]
    public GameObject UnlockedObject;

    [Header("Progress")]
    [Space(10)]
    public bool isPlayerNear = false;
    public bool isPlayerHasLeft = false;
    public bool isLocked;

    [Header("Border Expander")]
    public Transform border;
    public float Speed = 5;
    public float maxSize = 1.5f;

    public SectionManager sectionManager;
    [SerializeField]private controlUIMoney controlMoney;

    public AudioSource cashSound;

    private GameObject Player;
    private GameManager gameManager;


    // Start is called before the first frame update
    void Start()
    {
        unlocked();
        Player = GameObject.Find("Player");
        gameManager = GameManager.instance;
        size = 2;
        cashSound.enabled = false;
    }

    void unlocked()
    {
        if (!isLocked || unlockableAmount <= 0)
        {
            UnlockedObject.SetActive(true);
            LockedObject.SetActive(false);
            //GameManager.instance.UnlockedShopCount++;
        }
        
    }

    float size = 2;
    void borderExpander()
    {
        if (isPlayerNear && size < maxSize) size += Speed * Time.deltaTime;
        if (!isPlayerNear && size >= 2f) size -= Speed * Time.deltaTime;
        border.localScale = new Vector3(size, size, 1);
    }
    void Update()
    {
        moneyManagement();
        borderExpander();
    }
    void moneyManagement()
    {
        cashCounterText.text = unlockableAmount.ToString("0");
        if (isLocked && unlockableAmount < 1)
        {
            //Destroy(Instantiate(ParticalEffectForUnlocking, transform.position, Quaternion.identity), 5);
            LockedObject.SetActive(false);
            UnlockedObject.SetActive(true);
            ParticalEffectForUnlocking.Play();
            EvenManager.TriggerSFXOneShotPlayEvent(AudioID.SectionUnlock);
            //GameManager.instance.UnlockedShopCount++;
            //AudioManager.source.PlayOneShot(AudioManager.areaUnlock);
            isLocked = false;
        }

        if (isLocked && isPlayerNear)
            moneyReducer();

    }
    float x = 0.15f;
    void moneyReducer()
    {
        if (unlockableAmount > 0 && gameManager.maxCash > 0)
        {
            unlockableAmount -= reduceAmountSpeed * Time.deltaTime;
            gameManager.maxCash -= reduceAmountSpeed * Time.deltaTime;
            if(isPlayerNear)cashSound.enabled = true;
            if (unlockableAmount > 5)
            {
                if (x > 0)
                    x -= Time.deltaTime;
                if (x <= 0)
                {
                    //moneyDeductationUI();
                    x = 0.15f;
                }
            }

        }
        if (!isPlayerNear || (unlockableAmount > 0 && gameManager.maxCash < 1))
        {
            cashSound.enabled = false;
        }
    }
    void moneyDeductationUI()
    {
        GameObject UI = controlMoney.CashUI[controlMoney.CashUI.Count - 1].gameObject;
        controlMoney.CashUI.Remove(controlMoney.CashUI[controlMoney.CashUI.Count - 1]);
        UI.SetActive(true);

        UI.transform.position = Camera.main.WorldToScreenPoint(Player.transform.position + new Vector3(0, 1f, 0));
        controlMoney.SlotMachinePosition.position = Camera.main.WorldToScreenPoint(transform.position);

        LeanTween.moveLocal(UI, controlMoney.SlotMachinePosition.localPosition, .5f).setOnComplete(() =>
        {
            UI.SetActive(false);
            controlMoney.CashUI.Add(UI.transform);
        });
    }
    private void OnTriggerStay(Collider other)
    {
        try
        {
            if (other.gameObject.CompareTag("Player") && other.gameObject.GetComponent<movePlayer>().direction.magnitude < 0.1f)
            {
                Player = other.gameObject;
                isPlayerNear = true;
            }
            else if(other.gameObject.CompareTag("Player") && other.gameObject.GetComponent<movePlayer>().direction.magnitude > 0f)
            {
                isPlayerNear = false;
            }
        }
        catch
        {

        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (isPlayerNear)
        {
            Player = null;
            isPlayerNear = false;
            
        }
        cashSound.enabled = false;
    }
}
