using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlCustomer : MonoBehaviour
{





    public int NeedItemID;
    public GameObject money;

    public float waitTimer;
    public float MoneyDropOffset;
    public float TimerToTakeItemFromPlayer;
    public float exitTimeDelay = 1;
    public float cashCountToSpwan;

    public bool isTradeStarted;
    public bool isTradeRunning;
    public bool isPlayerNear;
    public bool isItemTookFromPlayer;
    public bool isTradeCompleted;

    public bool moneySpwanned;
    [HideInInspector] public moveCustomer moveCustomer;
    [HideInInspector] public SectionManager section;
    private animationHandler anim;
    private controlSpwanObject cso;
    void Start()
    {
        TimerToTakeItemFromPlayer = waitTimer;
        moveCustomer = GetComponent<moveCustomer>();
        anim = GetComponent<animationHandler>();
        cso = GetComponent<controlSpwanObject>();
    }

    private void Update()
    {
        desableOnTradeComplete();
        completeTransection();
    }
    void desableOnTradeComplete()
    {
        if (isItemTookFromPlayer && !isTradeRunning)
        {
            print("<color=green>Customer tooked item from player</color>");         
            if(LevelManager.instance.Level<2)FindObjectOfType<controlPlayerTutorial>().customerServed++;
            moveCustomer.resetCustomerList();
            moveCustomer.CSB.EnableUI();
            isTradeRunning = true;
        }
    }
    void spwanCashMoney()
    {
        for (int i = 0; i < cashCountToSpwan; i++)
        {
            GameObject c = Instantiate(money, new Vector3(transform.position.x, transform.position.y + 3, transform.position.z), Quaternion.identity);
            if (i >= cashCountToSpwan - 1) c.GetComponent<cash>().isPlay = true;
        }
        moveCustomer.CSB.EnableEmoji(true);
    }
    void completeTransection()
    {
        if(isTradeRunning && !moneySpwanned)
        {
            //Animation Trigger
            anim.playAnim(AnimationState.Victory);
            
            print("<color=green>Customer victory animation trigger</color>");            
            StartCoroutine(LeaveFromStore(exitTimeDelay));
            StartCoroutine(SpwanMoney(exitTimeDelay / 2));
            GameManager.instance.customerToServed += 1;
            moneySpwanned = true;
        }        
    }

    IEnumerator SpwanMoney(float time)
    {
        yield return new WaitForSeconds(time / 1.45f);
        spwanCashMoney();
        yield return new WaitForSeconds(time);
        anim.anim.SetBool("holding", true);
        cso.SetActiveObject(NeedItemID, true);

    }
    IEnumerator LeaveFromStore(float time)
    {
        

        yield return new WaitForSeconds(time);
        if (moneySpwanned && !isTradeCompleted)
        {
            moveCustomer.resetTarget();
            moveCustomer.isTradeComplete = true;
            moveCustomer.CSB.EmojiOut();
            print("<color=green>Customer reset target and left for exit</color>");
            anim.anim.SetBool("holding", true);
            cso.SetActiveObject(NeedItemID, true);
            isTradeCompleted = true;
        }
        
    }

    public void ResetControlSystem()
    {
        isTradeStarted = false;
        isTradeRunning = false;
        isPlayerNear = false;

        isItemTookFromPlayer = false;
        isTradeCompleted = false;
        moneySpwanned = false;

        cso.SetActiveObject(NeedItemID, false);
        anim.anim.SetBool("holding", false);
    }
}
