using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class moveEmp : MonoBehaviour
{
    [Header("Targets Positions")]
    public Transform InitalPosition;
    public Transform TargetToStore;
    public Transform TargetToCustomer;
    public Transform TargetToDustbin;

    [Header("Current State")]
    [Space(5)]
    public bool isWalkingToStore;
    public bool isWalkingToDustbin;
    public bool isWalkingToCustomer;
    public bool isWalkedToInitialPosition;

    [Header("Controller")]
    [Space(5)]
 

    public float rotationSmooth = 0.2f;
    private float turnSmoothVelocity;

    public Shop shop;
    public NavMeshAgent agent;
    private animationHandler anim;
    private controlEmp controlEmp;

    private void Start()
    {
        anim = GetComponent<animationHandler>();
        controlEmp = GetComponent<controlEmp>();
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        mover();
        whereToMove();
        getCustomerLocation();
        HoldAnimationTrigger();
        triggerRotation();
    }
    void mover()
    {
        if (agent.velocity.magnitude > 0.1f)
        {
            float targetAngle = Mathf.Atan2(agent.velocity.x, agent.velocity.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, rotationSmooth);
            transform.rotation = Quaternion.Euler(0, angle, 0);
        }
        anim.playAnim(AnimationState.Run, agent.velocity.magnitude);
    }
    void HoldAnimationTrigger()
    {
        if (controlEmp.Cart.Count > 0) anim.anim.SetBool("hold", true);
        if (controlEmp.Cart.Count <= 0) anim.anim.SetBool("hold", false);
    }
    void getCustomerLocation()
    {
        if (!TargetToCustomer && shop.CustomerOnStore.Count > 0)
        {
            if (shop.CustomerOnStore[0].isStartTrade && !shop.CustomerOnStore[0].control.isItemTookFromPlayer)
            {
                TargetToCustomer = shop.CustomerOnStore[0].transform;
            }
         /*   for (int i = 0; i < shop.CustomerOnStore.Count-1; i++)
            {
                if (shop.CustomerOnStore[i].isRechedCounterPosition && !shop.CustomerOnStore[i].control.isItemTookFromPlayer)
                {
                    TargetToCustomer = shop.CustomerOnStore[i].transform;
                    //shop.CustomerOnStore.Remove(shop.CustomerOnStore[i]);
                }
            }*/
        }
        if (TargetToCustomer)
        {
            if (TargetToCustomer.GetComponent<controlCustomer>().isItemTookFromPlayer)
            {
                TargetToCustomer = null;
            }
        }
    }
    void whereToMove()
    {
        if(TargetToCustomer && controlEmp.Cart.Count <= 0 && !isWalkingToStore)
        {            
            isWalkingToStore = true;
            isWalkingToCustomer =false;
            isWalkingToDustbin=false;
        }
        if (TargetToCustomer && controlEmp.Cart.Count > 0 && !isWalkingToCustomer)
        {
            isWalkingToDustbin =false;
            isWalkingToCustomer = true;
            isWalkingToStore = false;
        }
        if (!TargetToCustomer && controlEmp.Cart.Count > 0 && !isWalkingToDustbin)
        {
            isWalkingToDustbin = true;
            isWalkingToCustomer = false;
            isWalkingToStore = false;
        }
        if (!TargetToCustomer && controlEmp.Cart.Count <= 0)
        {
            isWalkingToDustbin = false;
            isWalkingToCustomer = false;
            isWalkingToStore = false;
        }

        if (!isWalkingToCustomer && !isWalkingToDustbin && !isWalkingToDustbin) agent.SetDestination(InitalPosition.position);
        if (isWalkingToCustomer) agent.SetDestination(TargetToCustomer.position);
        if (isWalkingToDustbin) agent.SetDestination(TargetToDustbin.position);
        if (isWalkingToStore) agent.SetDestination(TargetToStore.position);
    }
    void triggerRotation()
    {
        if (Vector3.Distance(transform.position, InitalPosition.position) <= 0.3f && !isWalkedToInitialPosition)
        {
            FaceTowardCounter(InitalPosition.eulerAngles.y, 0.2f);
             isWalkedToInitialPosition =true;
        }
        if (Vector3.Distance(transform.position, InitalPosition.position) > 0.3f && isWalkedToInitialPosition)
        {
            isWalkedToInitialPosition = false;
        }
    }

    void storeTrue()
    {
        isWalkingToStore = true;
    }
    void FaceTowardCounter(float direction, float time)
    {
        LeanTween.rotate(this.gameObject, new Vector3(0, direction, 0), time);
    }
}
